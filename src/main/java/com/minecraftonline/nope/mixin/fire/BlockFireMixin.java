package com.minecraftonline.nope.mixin.fire;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.nope.Nope;
import com.minecraftonline.nope.setting.SettingLibrary;
import net.minecraft.block.BlockFire;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.api.world.Location;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(BlockFire.class)
public class BlockFireMixin {

    /**
     * Inject just after the random chance tick (our call is more expensive than RNG),
     * to see whether we need to cancel this catching of fire.
     * This prevents direct fire spread.
     */
    @Inject(method = "catchOnFire",
            cancellable = true,
            at = @At(value = "INVOKE",
                    target = "Lnet/minecraft/world/World;getBlockState(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/state/IBlockState;",
                    ordinal = 0))
    private void injectCatchOnFire(World worldIn, BlockPos pos, int chance, Random random, int age, CallbackInfo ci) {
                if (!Nope.getInstance().getHostTree().lookupAnonymous(SettingLibrary.FIRE_EFFECT, getLoc(worldIn, pos))) {
            ci.cancel(); // Block the catching fire if fire effect turned off.
        }
    }

    /**
     * Fire also spreads more if there's more nearby fire.
     * We pretend that the block isn't an air block so that it thinks it's not valid.
     */
    @Redirect(method = "getNeighborEncouragement",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;isAirBlock(Lnet/minecraft/util/math/BlockPos;)Z", ordinal = 0))
    private boolean redirectIsValidFireBlock(World world, BlockPos pos) {
        return world.isAirBlock(pos) && Nope.getInstance().getHostTree().lookupAnonymous(SettingLibrary.FIRE_EFFECT, getLoc(world, pos));
    }

    private static Location<org.spongepowered.api.world.World> getLoc(World world, BlockPos pos) {
        return new Location<>((org.spongepowered.api.world.World) world, Vector3i.from(pos.getX(), pos.getY(), pos.getZ()));
    }
}
