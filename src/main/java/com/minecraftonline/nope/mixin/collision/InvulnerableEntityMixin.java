package com.minecraftonline.nope.mixin.collision;

import net.minecraft.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class InvulnerableEntityMixin {

    @Inject(method = "isImmuneToExplosions", at = @At("HEAD"), cancellable = true)
    public void onIsImmuneToExplosions(CallbackInfoReturnable<Boolean> cir) {
        // make all invulnerable entities immune to explosions
        if (((Entity)(Object)this).getIsInvulnerable()) cir.setReturnValue(((Entity)(Object)this).getIsInvulnerable());
    }
}