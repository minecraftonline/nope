/*
 * MIT License
 *
 * Copyright (c) 2021 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.command;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.nope.Nope;
import com.minecraftonline.nope.key.zonewand.ZoneWandHandler;
import com.minecraftonline.nope.arguments.NopeArguments;
import com.minecraftonline.nope.command.common.CommandNode;
import com.minecraftonline.nope.command.common.LambdaCommandNode;
import com.minecraftonline.nope.host.Host;
import com.minecraftonline.nope.host.SphereHost;
import com.minecraftonline.nope.host.CuboidHost;
import com.minecraftonline.nope.permission.Permissions;
import com.minecraftonline.nope.util.Format;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class MoveCommand extends LambdaCommandNode {

  MoveCommand(CommandNode parent) {
    super(parent,
        Permissions.COMMAND_EDIT,
        Text.of("Redefine the boundaries of a zone"),
        "move");

    addCommandElements(
        NopeArguments.host(Text.of("host")),
        NopeArguments.zoneLocation(Text.of("selection"))
    );
    setExecutor((src, args) -> {
      Host host = args.requireOne("host");
      ZoneWandHandler.Selection selection = args.requireOne("selection");

      if (!(host instanceof CuboidHost || host instanceof SphereHost)) {
        src.sendMessage(Format.error("You can only move volumetric zones!"));
        return CommandResult.empty();
      }

      Vector3i min = selection.getMin();
      Vector3i max = selection.getMax();

      World world = selection.getWorld();

      if (world == null) {
        throw new IllegalStateException("World was null where it never should be!");
      }

      // Remove the host that's moving
      Host removed;
      try {
        removed = Nope.getInstance().getHostTree().removeZone(host.getName());
      } catch (IllegalArgumentException e) {
        src.sendMessage(Format.error("Could not move zone: " + e.getMessage()));
        return CommandResult.empty();
      }

      // Add the new one
      Host created;
      try {
        if (host instanceof SphereHost) {
          created = Nope.getInstance().getHostTree().addSphereZone(
              host.getName(),
              world.getUniqueId(),
              min,
              max,
              host.getPriority()
          );
        } else {
          created = Nope.getInstance().getHostTree().addCuboidZone(
              host.getName(),
              world.getUniqueId(),
              min,
              max,
              host.getPriority()
          );
        }
        if (created == null) {
          src.sendMessage(Format.error("Could not create zone"));
          reAddHost(removed, src);
          return CommandResult.empty();
        }
        created.putAll(removed.getAll());

      } catch (IllegalArgumentException e) {
        src.sendMessage(Format.error("Could not move zone: " + e.getMessage()));
        reAddHost(removed, src);
        return CommandResult.empty();
      }

      Nope.getInstance().saveState();
      if (host instanceof SphereHost) {
        src.sendMessage(Format.success(String.format(
            "Moved zone %s to (%d, %d, %d) with radius %s in world %s",
            host.getName(),
            min.getX(), min.getY(), min.getZ(),
            ((SphereHost) created).getRadius(),
            world.getName()
        )));
      } else {
        src.sendMessage(Format.success(String.format(
            "Moved zone %s to (%d, %d, %d) <-> (%d, %d, %d) in world %s",
            host.getName(),
            min.getX(), min.getY(), min.getZ(),
            max.getX(), max.getY(), max.getZ(),
            world.getName()
        )));
      }

      return CommandResult.success();
    });
  }

  private void reAddHost(Host host, CommandSource src) {
    Host reAdded;
    try {
      if (host instanceof SphereHost) {
        SphereHost sphereHost = (SphereHost) host;
        reAdded = Nope.getInstance().getHostTree().addSphereZone(
            host.getName(),
            host.getWorldUuid(),
            new Vector3i(sphereHost.getX(), sphereHost.getY(), sphereHost.getZ()),
            sphereHost.getRadius(),
            host.getPriority());
      } else {
        CuboidHost cuboidHost = (CuboidHost) host;
        reAdded = Nope.getInstance().getHostTree().addCuboidZone(
            host.getName(),
            host.getWorldUuid(),
            new Vector3i(cuboidHost.getMinX(), cuboidHost.getMinY(), cuboidHost.getMinZ()),
            new Vector3i(cuboidHost.getMaxX(), cuboidHost.getMaxY(), cuboidHost.getMaxZ()),
            host.getPriority());
      }
      if (reAdded == null) {
        src.sendMessage(Format.error("Severe: The host in transit could not be recovered"));
        Nope.getInstance().getLogger()
            .error(String.format("Host %s was requested to move by %s. "
                    + "The move failed and the original host could not be recovered.",
            host.getName(), src.getName()));
      }
    } catch (IllegalArgumentException e) {
      src.sendMessage(Format.error("Severe: The host in transit could not be recovered"));
    }
  }
}
