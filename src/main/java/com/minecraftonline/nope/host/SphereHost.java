/*
 * MIT License
 *
 * Copyright (c) 2020 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.host;

import com.minecraftonline.nope.structures.Sphere;

public abstract class SphereHost extends HostImpl implements Sphere {

  protected final int x;

  protected final int y;

  protected final int z;

  protected final double radius;

  /**
   * Default constructor.
   * @param name the name
   * @param xmin the first x value
   * @param xmax the second x value
   * @param ymin the first y value
   * @param ymax the second y value
   * @param zmin the first z value
   * @param zmax the second z value
   */
  public SphereHost(String name, int x1, int x2, int y1, int y2, int z1, int z2) {
      this(name, x1, y1, z1, Sphere.distanceBetweenPoints(x1, y1, z1, x2, y2, z2) + 0.5);
  }

  /**
   * Default constructor.
   * @param name the name
   * @param x the center x value
   * @param y the center y value
   * @param z the center z value
   * @param radius the radius of the sphere
   */
  public SphereHost(String name,
                    int x,
                    int y,
                    int z,
                    double radius) {
    super(name);
    this.x = x;
    this.y = y;
    this.z = z;
    this.radius = radius;
  }

  @Override
  public int getX() {
    return x;
  }

  @Override
  public int getY() {
    return y;
  }

  @Override
  public int getZ() {
    return z;
  }

  @Override
  public double getRadius() {
    return radius;
  }

  @Override
  public int getMinX() {
    return x - (int) radius;
  }

  @Override
  public int getMaxX() {
    return x + (int) radius;
  }

  @Override
  public int getMinY() {
    return y - (int) radius;
  }

  @Override
  public int getMaxY() {
    return y + (int) radius;
  }

  @Override
  public int getMinZ() {
    return z - (int) radius;
  }

  @Override
  public int getMaxZ() {
    return z + (int) radius;
  }

}
