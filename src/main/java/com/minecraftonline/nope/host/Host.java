/*
 * MIT License
 *
 * Copyright (c) 2020 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.host;

import com.minecraftonline.nope.setting.Setting;
import com.minecraftonline.nope.setting.SettingKey;
import com.minecraftonline.nope.setting.SettingLibrary;
import com.minecraftonline.nope.setting.SettingMap;
import com.minecraftonline.nope.setting.SettingValue;
import lombok.Getter;
import lombok.Setter;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.context.Context;
import org.spongepowered.api.world.Locatable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.UUID;

/**
 * A storage class for setting assignments.
 */
public interface Host {

  String getName();

  Context getContext();
  
  void setParent(Host parent);

  Host getParent();

  int getPriority();

  /**
   * Assign a value to a setting for this Host.
   *
   * @param key   The setting
   * @param value The value to assign
   * @param <A>   The type of value this is
   * @return The value which was saved prior
   * @see SettingLibrary
   */
  @Nonnull
  <A> Optional<A> put(SettingKey<A> key, SettingValue<A> value);

  /**
   * Assigns all the values in the map under the given settings.
   *
   * @param settings all settings
   * @see SettingLibrary
   */
  void putAll(SettingMap settings);

  /**
   * Retrieves the value associated with a setting under this Host.
   *
   * @param key the setting which keys the value
   * @param <A> the type of value stored
   * @return the value
   * @see SettingLibrary
   */
  @Nonnull
  <A> Optional<SettingValue<A>> get(SettingKey<A> key);

  /**
   * Retrieves all values associated with settings under this Host.
   * The types are hidden, so it is suggested you use {@link #get(SettingKey)}
   * wherever possible.
   *
   * @return a copy of all the setting assignments
   * @see SettingLibrary
   */
  @Nonnull
  SettingMap getAll();

  /**
   * Get the data associated on this host, regardless of the
   * {@link com.minecraftonline.nope.setting.SettingValue.Target}.
   *
   * @param key the key for which to search
   * @param <A> the type of data to retrieve
   * @return the data
   */
  @Nonnull
  <A> A getData(SettingKey<A> key);

  /**
   * Get the data associated on this host.
   *
   * @param key    the key for which to search
   * @param player the player to test for targeting
   * @param <A>    the type of data to retrieve
   * @return the data
   */
  <A> A getData(SettingKey<A> key, Player player);

  void setPriority(int priority) throws IllegalArgumentException;

  /**
   * Check if a setting is assigned for this Host.
   *
   * @param setting the setting to check for existence
   * @return true if exists
   * @see SettingLibrary
   */
  boolean has(SettingKey<?> setting);

  /**
   * Removes any value associated with the given
   * {@link SettingKey} from this host.
   *
   * @param key Key to remove the mapping for.
   * @return The no longer associated {@link SettingValue}, or null, if nothing was removed.
   */
  @Nullable
  <A> SettingValue<A> remove(SettingKey<A> key);

  /**
   * Clears all the {@link Setting} assignments.
   */
  void clear();

  /**
   * Check if a Sponge locatable exists within this host.
   * This is the same as calling this method with the
   * locatable's location with {@link #encompasses(Location)}
   *
   * @param spongeLocatable the locatable
   * @return true if within the host
   */
  boolean encompasses(Locatable spongeLocatable);

  /**
   * Check if a Sponge Location exists within this host.
   *
   * @param spongeLocation the location
   * @return true if within the host
   */
  boolean encompasses(Location<World> spongeLocation);

  /**
   * Get the UUID of the Sponge Minecraft
   * world to which this Host is associated.
   *
   * @return the world's UUID, or null if the host is not associated with a world
   */
  @Nullable
  UUID getWorldUuid();

}
