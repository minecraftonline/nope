/*
 * MIT License
 *
 * Copyright (c) 2021 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.structures;

public interface Sphere extends Volume {

  /**
   * Get center X value.
   *
   * @return x
   */
  int getX();
  /**
   * Get center Y value.
   *
   * @return y
   */
  int getY();

  /**
   * Get center Z value.
   *
   * @return z
   */
  int getZ();

  /**
   * Get radius.
   *
   * @return radius
   */
  double getRadius();

  static double distanceBetweenPoints(int x1, int y1, int z1, int x2, int y2, int z2)
  {
    double distance = Math.sqrt(
      (x1 - x2) * (x1 - x2) +
      (y1 - y2) * (y1 - y2) +
      (z1 - z2) * (z1 - z2)
    );
    return distance;
  }

  default double distanceFromCenter(int x, int y, int z)
  {
    return distanceBetweenPoints(x, y, z, this.getX(), this.getY(), this.getZ());
  }

  /**
   * Check if this volume contains a point, given three
   * cartesian coordinates.
   *
   * @param x x value
   * @param y y value
   * @param z z value
   * @return true if the point is contained
   */
  default boolean contains(int x, int y, int z) {
    return this.distanceFromCenter(x, y, z) <= this.getRadius();
  }

  /**
   * Check if this sphere contains a volume
   *
   * @param other a volume
   * @return true if contains
   */
  default boolean contains(Volume other) {
    if (other instanceof Sphere) {
      return this.contains((Sphere) other);
    } else if (other instanceof Cuboid) {
      return this.contains((Cuboid) other);
    }
    return false;
  }

  /**
   * Check if this sphere contains another sphere
   *
   * @param other another sphere
   * @return true if contains
   */
  default boolean contains(Sphere other) {
    return this.distanceFromCenter(other.getX(), other.getY(), other.getZ()) + other.getRadius() <= this.getRadius();
  }

  /**
   * Check if this sphere contains a cuboid
   *
   * @param other cuboid
   * @return true if contains
   */
  default boolean contains(Cuboid other) {
    // find corner that is furtherest away from center of sphere
    int x = this.getX() - other.getMinX() >= this.getX() - other.getMaxX() ? other.getMinX() : other.getMaxX();
    int y = this.getY() - other.getMinY() >= this.getY() - other.getMaxY() ? other.getMinY() : other.getMaxY();
    int z = this.getZ() - other.getMinZ() >= this.getZ() - other.getMaxZ() ? other.getMinZ() : other.getMaxZ();

    return this.distanceFromCenter(x, y, z) <= this.getRadius();
  }

  /**
   * Check if this sphere intersects with a volume
   *
   * @param other a volume
   * @return true if intersects
   */
  default boolean intersects(Volume other) {
    if (other instanceof Sphere) {
      return this.intersects((Sphere) other);
    } else if (other instanceof Cuboid) {
      return this.intersects((Cuboid) other);
    }
    return false;
  }

  /**
   * Check if this sphere intersects with another sphere
   *
   * @param other another sphere
   * @return true if intersects
   */
  default boolean intersects(Sphere other) {
    return this.distanceFromCenter(other.getX(), other.getY(), other.getZ()) <= this.getRadius() + other.getRadius();
  }

  /**
   * Check if this sphere intersects with a cuboid
   *
   * @param other a cuboid
   * @return true if intersects
   */
  default boolean intersects(Cuboid other) {
    int x = Math.max(other.getMinX(), Math.min(this.getX(), other.getMaxX()));
    int y = Math.max(other.getMinY(), Math.min(this.getY(), other.getMaxY()));
    int z = Math.max(other.getMinZ(), Math.min(this.getZ(), other.getMaxZ()));

    return this.distanceFromCenter(x, y, z) <= this.getRadius();
  }

}
