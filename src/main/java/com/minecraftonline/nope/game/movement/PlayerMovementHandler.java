/*
 * MIT License
 *
 * Copyright (c) 2021 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.game.movement;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.minecraftonline.nope.Nope;
import com.minecraftonline.nope.host.HostImpl;
import com.minecraftonline.nope.host.Host;
import com.minecraftonline.nope.setting.SettingLibrary;
import com.minecraftonline.nope.setting.SettingLibrary.Movement;
import com.minecraftonline.nope.structures.Volume;
import com.minecraftonline.nope.util.EffectsUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.scoreboard.Team;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlot;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlots;
import org.spongepowered.api.scoreboard.objective.Objective;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.title.Title;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nonnull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

public class PlayerMovementHandler {

  @Data
  @NoArgsConstructor
  private static class PlayerMovementData {
    private long visualsTimeStamp = System.currentTimeMillis();
    private String lastSentMessage = "Few foxes fly farther than Florence";
    private boolean viewing = false;
    private boolean nextTeleportVerificationNeeded = false;
    private Predicate<MoveEntityEvent.Teleport> nextTeleportCanceller = event -> false;
    private long nextTeleportCancellationExpiry = System.currentTimeMillis();
  }

  private static final long MESSAGE_COOLDOWN_MILLISECONDS = 1000;

  private final Map<UUID, PlayerMovementData> movementDataMap = Maps.newConcurrentMap();
  private final Map<String, Scoreboard> zoneScoreboardMap = Maps.newConcurrentMap();

  public void addHostViewer(UUID playerUuid) {
    movementDataMap.get(playerUuid).setViewing(true);
  }

  public boolean isHostViewer(UUID playerUuid) {
    return movementDataMap.get(playerUuid).isViewing();
  }

  public void removeHostViewer(UUID playerUuid) {
    movementDataMap.get(playerUuid).setViewing(false);
  }

  /**
   * Set up cancellation of the next teleport, only if the canceller test
   * is true. Whether or not the canceller resolves true or false,
   * the subsequent teleport is allowed without any checks.
   * (Only one check on one teleport event is made.)
   *
   * @param playerUuid the uuid of the player
   * @param canceller the cancellation test
   * @param timeoutMillis the timeout in milliseconds, after which the
   *                      any cancellation behavior is ignored (used in case
   *                      an expected teleport event actually is never thrown
   *                      for whatever reason)
   */
  public void cancelNextTeleportIf(@Nonnull UUID playerUuid,
                                   @Nonnull Predicate<MoveEntityEvent.Teleport> canceller,
                                   int timeoutMillis) {
    PlayerMovementData data = movementDataMap.get(playerUuid);
    data.setNextTeleportVerificationNeeded(true);
    data.setNextTeleportCanceller(canceller);
    data.setNextTeleportCancellationExpiry(System.currentTimeMillis() + timeoutMillis);
  }

  /**
   * Identify if this teleport event should be cancelled based on
   * previous set cancellation behavior. If special behavior is needed,
   * it also prepares the user's system for the next event.
   *
   * @param playerUuid the player uuid
   * @param event the event that was thrown
   * @return true if it should be cancelled
   */
  public boolean resolveTeleportCancellation(@Nonnull UUID playerUuid, @Nonnull MoveEntityEvent.Teleport event) {
    PlayerMovementData data = movementDataMap.get(playerUuid);
    if (!data.isNextTeleportVerificationNeeded()) {
      return false;
    }
    movementDataMap.get(playerUuid).setNextTeleportVerificationNeeded(false);
    if (System.currentTimeMillis() < data.getNextTeleportCancellationExpiry()) {
      return movementDataMap.get(playerUuid).nextTeleportCanceller.test(event);
    } else {
      return false;
    }
  }

  public void logIn(UUID playerUuid) {
    movementDataMap.put(playerUuid, new PlayerMovementData());

    // apply any scoreboard displays
    if (!Sponge.getServer().getPlayer(playerUuid).isPresent()) return;
    Player player = Sponge.getServer().getPlayer(playerUuid).get();

    if (!Sponge.getServer().getServerScoreboard().isPresent()) return;
    Scoreboard originalScoreboard = Sponge.getServer().getServerScoreboard().get();

    List<Host> zones = new LinkedList<>(Nope.getInstance()
        .getHostTree()
        .getContainingHosts(player.getLocation()));

    for (Host zone : zones) {
      applyScoreboardDisplay(player, zone, originalScoreboard);
    }
  }

  public void logOut(UUID playerUuid) {
    movementDataMap.remove(playerUuid);
  }

  public boolean applyScoreboardDisplay(Player player, Host zone, Scoreboard originalScoreboard) {
    String hostName = zone.getName();
    if (zoneScoreboardMap.containsKey(hostName)) {
      player.setScoreboard(zoneScoreboardMap.get(hostName));
      return true;
    }

    Optional<Objective> belowNameObjective = Optional.empty();
    Optional<Objective> listObjective      = Optional.empty();
    Optional<Objective> sidebarObjective   = Optional.empty();
    String belowNameObjectiveName = zone.getData(SettingLibrary.SCOREBOARD_DISPLAY_BELOW_NAME, player);
    String listObjectiveName      = zone.getData(SettingLibrary.SCOREBOARD_DISPLAY_LIST,       player);
    String sidebarObjectiveName   = zone.getData(SettingLibrary.SCOREBOARD_DISPLAY_SIDEBAR,    player);
    if (!belowNameObjectiveName.isEmpty()) belowNameObjective = originalScoreboard.getObjective(belowNameObjectiveName);
    if (!listObjectiveName.isEmpty())      listObjective      = originalScoreboard.getObjective(listObjectiveName);
    if (!sidebarObjectiveName.isEmpty())   sidebarObjective   = originalScoreboard.getObjective(sidebarObjectiveName);
    if (belowNameObjective.isPresent() || listObjective.isPresent() || sidebarObjective.isPresent()) {
      // create a custom scoreboard, otherwise changing the display slot on the original will effect players outside the zone too
      Scoreboard zoneScoreboard = Scoreboard.builder()
              .objectives(new ArrayList<>(originalScoreboard.getObjectives()))
              // teams can only belong to one scoreboard
              .build();
      // add objectives to displays if present, or existing ones if not
      zoneScoreboard.updateDisplaySlot(belowNameObjective.orElse(
              originalScoreboard.getObjective(DisplaySlots.BELOW_NAME).orElse(null)), DisplaySlots.BELOW_NAME);
      zoneScoreboard.updateDisplaySlot(listObjective.orElse(
              originalScoreboard.getObjective(DisplaySlots.LIST).orElse(null)), DisplaySlots.LIST);
      zoneScoreboard.updateDisplaySlot(sidebarObjective.orElse(
              originalScoreboard.getObjective(DisplaySlots.SIDEBAR).orElse(null)), DisplaySlots.SIDEBAR);
      zoneScoreboardMap.put(hostName, zoneScoreboard);
      player.setScoreboard(zoneScoreboard);
      return true;
    }
    return false;
  }

  public void setDefaultScoreboard(Player player)
  {
    // get the default scoreboard display for this player
    // first checking their team, then the server
    Optional<Scoreboard> scoreboard = Sponge.getServer().getServerScoreboard();
    if (scoreboard.isPresent()) {
      Optional<Team> team = scoreboard.get().getMemberTeam(Text.of(player.getFriendlyIdentifier()));
      if (team.isPresent()) {
        Optional<Scoreboard> teamScoreboard = team.get().getScoreboard();
        if (teamScoreboard.isPresent()) {
          player.setScoreboard(teamScoreboard.get());
          return;
        }
      }

      player.setScoreboard(scoreboard.get());
    }
  }

  public boolean tryPassThreshold(Player player,
                               Location<World> first,
                               Location<World> last,
                               boolean natural) {
    boolean cancel = false;
    boolean visual = false;

    /* Find these applicable values for exiting or entering */
    SettingLibrary.Movement movementData;
    Movement messageMovement;
    Text message;
    Text title;
    Text subtitle;
    Text actionbar;
    boolean expired = movementDataMap.get(player.getUniqueId()).getVisualsTimeStamp()
        + MESSAGE_COOLDOWN_MILLISECONDS < System.currentTimeMillis();
    String lastSentMessage = movementDataMap.get(player.getUniqueId()).getLastSentMessage();

    // moving internally inside a zone
    // or another zone with the same restriction
    Movement firstMovementType = Nope.getInstance().getHostTree().lookup(
      SettingLibrary.INTERNAL_MOVEMENT,
      player,
      first);
    Movement lastMovementType = Nope.getInstance().getHostTree().lookup(
      SettingLibrary.INTERNAL_MOVEMENT,
      player,
      last);
    if (  ((firstMovementType == Movement.NONE || firstMovementType == Movement.UNNATURAL)
       && ( lastMovementType  == Movement.NONE || lastMovementType  == Movement.UNNATURAL) && natural)
       || ((firstMovementType == Movement.NONE || firstMovementType == Movement.NATURAL)
       && ( lastMovementType  == Movement.NONE || lastMovementType  == Movement.NATURAL) && !natural))
    {
      message = Nope.getInstance().getHostTree().lookup(
        SettingLibrary.INTERNAL_MOVEMENT_DENY_MESSAGE,
        player,
        first);
      title = Nope.getInstance().getHostTree().lookup(
        SettingLibrary.INTERNAL_MOVEMENT_DENY_TITLE,
        player,
        first);
      subtitle = Nope.getInstance().getHostTree().lookup(
        SettingLibrary.INTERNAL_MOVEMENT_DENY_SUBTITLE,
        player,
        first);

      if (!message.isEmpty() && (expired || !lastSentMessage.equals(message.toPlain()))) {
        player.sendMessage(message);
        movementDataMap.get(player.getUniqueId()).setLastSentMessage(message.toPlain());
        visual = true;
      }
      if (!title.isEmpty() || !subtitle.isEmpty()) {
        player.sendTitle(Title.builder().title(title).subtitle(subtitle).build());
        visual = true;
      }

      /* Update message time (for reduced spamming) */
      if (visual) {
        // set here as the other spot may not be reached if not entering/exiting a zone
        movementDataMap.get(player.getUniqueId()).setVisualsTimeStamp(System.currentTimeMillis());
        visual = false;
      }

      cancel = true;
    }

    List<Host> exiting = new LinkedList<>(Nope.getInstance()
        .getHostTree()
        .getContainingHosts(first));
    List<Host> entering = new LinkedList<>(Nope.getInstance()
        .getHostTree()
        .getContainingHosts(last));
    Set<Host> unchanged = Sets.newHashSet(exiting);
    unchanged.retainAll(entering);
    exiting.removeAll(unchanged);
    entering.removeAll(unchanged);

    /* Call it quits if we aren't moving anywhere special */
    if (exiting.isEmpty() && entering.isEmpty()) {
      return cancel;
    }

    boolean resetScoreboard = false;

    // Ignore sorting by priority for now, it doesn't really matter

    /* Exiting */
    if (!cancel) {
      for (int i = exiting.size() - 1; i >= 0; i--) {
        movementData = exiting.get(i).getData(SettingLibrary.EXIT, player);
        if (movementData.equals(SettingLibrary.Movement.NONE)
            || (movementData.equals(SettingLibrary.Movement.NATURAL) && !natural)
            || (movementData.equals(SettingLibrary.Movement.UNNATURAL) && natural)) {
          cancel = true;
          message = exiting.get(i).getData(SettingLibrary.EXIT_DENY_MESSAGE, player);
          title = exiting.get(i).getData(SettingLibrary.EXIT_DENY_TITLE, player);
          subtitle = exiting.get(i).getData(SettingLibrary.EXIT_DENY_SUBTITLE, player);
          actionbar = exiting.get(i).getData(SettingLibrary.EXIT_DENY_ACTIONBAR, player);
        } else {
          messageMovement = exiting.get(i).getData(SettingLibrary.FAREWELL_MOVEMENT);
          if (messageMovement.equals(SettingLibrary.Movement.ALL)
              || (messageMovement.equals(SettingLibrary.Movement.NATURAL) && natural)
              || (messageMovement.equals(SettingLibrary.Movement.UNNATURAL) && !natural)) {
            message = exiting.get(i).getData(SettingLibrary.FAREWELL, player);
            title = exiting.get(i).getData(SettingLibrary.FAREWELL_TITLE, player);
            subtitle = exiting.get(i).getData(SettingLibrary.FAREWELL_SUBTITLE, player);
            actionbar = exiting.get(i).getData(SettingLibrary.FAREWELL_ACTIONBAR, player);
          } else {
            message = Text.EMPTY;
            title = Text.EMPTY;
            subtitle = Text.EMPTY;
            actionbar = Text.EMPTY;
          }

          // clear scoreboard view for this zone if it was set
          if (zoneScoreboardMap.containsKey(exiting.get(i).getName())) {
            resetScoreboard = true;
          }
        }

        if (!message.isEmpty() && (expired || !lastSentMessage.equals(message.toPlain()))) {
          player.sendMessage(message);
          movementDataMap.get(player.getUniqueId()).setLastSentMessage(message.toPlain());
          visual = true;
        }
        if (!title.isEmpty() || !subtitle.isEmpty() || !actionbar.isEmpty()) {
          player.sendTitle(Title.builder().title(title).subtitle(subtitle).actionBar(actionbar).build());
          visual = true;
        }

        if (exiting.get(i) instanceof Volume && isHostViewer(player.getUniqueId()) && expired) {
          EffectsUtil.showVolume((Volume) exiting.get(i), player, 5);
          visual = true;
        }
      }
    }

    /* Entering */
    if (!cancel) {  // Only entering if we could exit from before
      for (int i = entering.size() - 1; i >= 0; i--) {
        movementData = entering.get(i).getData(SettingLibrary.ENTRY, player);
        if (movementData.equals(SettingLibrary.Movement.NONE)
            || (movementData.equals(SettingLibrary.Movement.NATURAL) && !natural)
            || (movementData.equals(SettingLibrary.Movement.UNNATURAL) && natural)) {
          cancel = true;
          message = entering.get(i).getData(SettingLibrary.ENTRY_DENY_MESSAGE, player);
          title = entering.get(i).getData(SettingLibrary.ENTRY_DENY_TITLE, player);
          subtitle = entering.get(i).getData(SettingLibrary.ENTRY_DENY_SUBTITLE, player);
          actionbar = entering.get(i).getData(SettingLibrary.ENTRY_DENY_ACTIONBAR, player);
        } else {
          messageMovement = entering.get(i).getData(SettingLibrary.GREETING_MOVEMENT);
          if (messageMovement.equals(SettingLibrary.Movement.ALL)
              || (messageMovement.equals(SettingLibrary.Movement.NATURAL) && natural)
              || (messageMovement.equals(SettingLibrary.Movement.UNNATURAL) && !natural)) {
            message = entering.get(i).getData(SettingLibrary.GREETING, player);
            title = entering.get(i).getData(SettingLibrary.GREETING_TITLE, player);
            subtitle = entering.get(i).getData(SettingLibrary.GREETING_SUBTITLE, player);
            actionbar = entering.get(i).getData(SettingLibrary.GREETING_ACTIONBAR, player);
          } else {
            message = Text.EMPTY;
            title = Text.EMPTY;
            subtitle = Text.EMPTY;
            actionbar = Text.EMPTY;
          }

          // show scoreboard view for this zone if set
          if (Sponge.getServer().getServerScoreboard().isPresent()) {
            if (applyScoreboardDisplay(player, entering.get(i), Sponge.getServer().getServerScoreboard().get())) {
              resetScoreboard = false;
            }
          }
        }
        if (!message.isEmpty() && (expired || !lastSentMessage.equals(message.toPlain()))) {
          player.sendMessage(message);
          movementDataMap.get(player.getUniqueId()).setLastSentMessage(message.toPlain());
          visual = true;
        }
        if (!title.isEmpty() || !subtitle.isEmpty() || !actionbar.isEmpty()) {
          player.sendTitle(Title.builder().title(title).subtitle(subtitle).actionBar(actionbar).build());
          visual = true;
        }

        if (entering.get(i) instanceof Volume && isHostViewer(player.getUniqueId()) && expired) {
          EffectsUtil.showVolume((Volume) entering.get(i), player, 5);
          visual = true;
        }
      }
    }

    if (resetScoreboard) {
      setDefaultScoreboard(player);
    }

    /* Update message time (for reduced spamming) */
    if (visual) {
      movementDataMap.get(player.getUniqueId()).setVisualsTimeStamp(System.currentTimeMillis());
    }

    /* Perform cancellation behavior */

    // This is a quick fix for managing cancellation while riding vehicles
    if (cancel && player.getVehicle().isPresent()) {
      // Dismount so the even can be cancelled properly
      Entity vehicle = player.getVehicle().get();
      player.setVehicle(null);
      // Move the vehicle back to the player so the vehicle doesn't get stuck
      vehicle.setTransform(player.getTransform());
    }

    return cancel;
  }

}
